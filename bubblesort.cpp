#include "bubblesort.h"
BubbleSort::BubbleSort() : Sorter()
{
    i_index = 0;
    j_index = 0;
}

BubbleSort::BubbleSort(BubbleSort *b) : Sorter(b)
{
    i_index = b->i_index;
    j_index = b->j_index;
    length = item->Length();
}

BubbleSort::~BubbleSort()
{
    if(item != 0)
    {
        delete item;
        item = 0;
    }
}

void BubbleSort::SortToEnd()
{
    while(i_index < length - 1)
    {
        if(j_index < length - 1)
        {
            Item *a = item->GetItem(j_index),
                 *b = item->GetItem(j_index + 1);
            steps++;    //tăng số bước duyệt
            if(!IsRight(a,b))   //Kiểm tra vị trí a và b là đúng hay sai
            {
                swapped++;  //tăng số lần đổi chỗ
                //Đổi chỗ a và b
                a->value +=b->value; //a=a+b
                b->value = a->value - b->value; //b =(a+b) - b = a
                a->value = a->value - b->value; //a =(a+b) - a = b
            }
            j_index++;
        }
        else
        {
            j_index = 0;
            i_index++;
        }
    }
}

int BubbleSort::Sort()
{
    if(i_index < length -1)
    {
        if(j_index < length - 1)
        {
            Item *a = item->GetItem(j_index),
                 *b = item->GetItem(j_index + 1);
            steps++;    //tăng số bước duyệt
            if(!IsRight(a,b))   //Kiểm tra vị trí a và b là đúng hay sai
            {
                swapped++;  //tăng số lần đổi chỗ
                //Đổi chỗ a và b
                a->value +=b->value; //a=a+b
                b->value = a->value - b->value; //b =(a+b) - b = a
                a->value = a->value - b->value; //a =(a+b) - a = b

                b->status = 1;
                a->status = 2;
            }
            j_index++;
        }
        else
        {
            j_index = 0;
            i_index++;
        }
        return 0;
    }else
        return 1;
}

void BubbleSort:: Reset(Item *i,bool ascending)
{
    if(item != 0)
        delete this->item;
    item = new Item(i);
    this->ascending = ascending;
    i_index = 0;
    j_index = 0;
    length = item->Length();

    steps = 0;
    swapped = 0;
}
