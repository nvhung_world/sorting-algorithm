#ifndef SORTER_H
#define SORTER_H
#include "item.h"
class Sorter
{
public:
    Item *item;
    int swapped;    //số lần hoán đổi
    int steps;      //số bước duyệt
    bool ascending; //kiểu sắp xếp: true=tăng dần, false = giảm dần
    //Hàm tạo
    Sorter()
    {
        item = 0;
        steps = 0;
        swapped = 0;
    }
    //Hàm tạo sao chép 1 Sorter
    Sorter(Sorter *s)
    {
        item = new Item(s->item);
        steps = s->steps;
        swapped = s->swapped;
        ascending = s->ascending;
    }
    virtual ~Sorter()
    {
        if(item != 0)
        {
            delete item;
            item = 0;
        }
    }
    //

    //Kiểm tra thứ tự của A và B đã đúng vị trí chưa
    bool IsRight(Item *a, Item *b)
    {
        return (ascending == ( a->value <= b->value));
    }
    bool IsRight(int a, int b)
    {
        return (ascending == ( a <= b));
    }
    //Sắp xếp toàn bộ dãy cho tới khi hoàn thành
    virtual void SortToEnd()
    {

    }
    //Sắp xếp bước từng bước 1
    virtual int Sort()
    {
        return 0;
    }
    //
    virtual void Reset(Item *i,bool ascending = true)
    {
        if(item != 0)
            delete this->item;
        item = new Item(i);
        this->ascending = ascending;
        steps = 0;
        swapped = 0;
    }
};

#endif // SORTER_H
