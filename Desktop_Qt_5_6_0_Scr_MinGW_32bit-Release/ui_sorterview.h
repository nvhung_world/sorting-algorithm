/********************************************************************************
** Form generated from reading UI file 'sorterview.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SORTERVIEW_H
#define UI_SORTERVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SorterView
{
public:
    QWidget *widget;
    QLabel *name;
    QLabel *swapped;
    QLabel *steps;
    QLabel *TexLable;
    QLabel *timeLable;
    QLabel *hieu_Xuat;
    QGraphicsView *graphicsView;

    void setupUi(QFrame *SorterView)
    {
        if (SorterView->objectName().isEmpty())
            SorterView->setObjectName(QStringLiteral("SorterView"));
        SorterView->resize(315, 250);
        SorterView->setFrameShape(QFrame::StyledPanel);
        SorterView->setFrameShadow(QFrame::Raised);
        widget = new QWidget(SorterView);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(0, 190, 315, 60));
        widget->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 127);"));
        name = new QLabel(widget);
        name->setObjectName(QStringLiteral("name"));
        name->setGeometry(QRect(80, 0, 181, 20));
        name->setStyleSheet(QStringLiteral("color: rgb(85, 85, 255);"));
        swapped = new QLabel(widget);
        swapped->setObjectName(QStringLiteral("swapped"));
        swapped->setGeometry(QRect(10, 20, 121, 20));
        steps = new QLabel(widget);
        steps->setObjectName(QStringLiteral("steps"));
        steps->setGeometry(QRect(10, 40, 121, 20));
        TexLable = new QLabel(widget);
        TexLable->setObjectName(QStringLiteral("TexLable"));
        TexLable->setGeometry(QRect(160, 20, 81, 21));
        TexLable->setWordWrap(true);
        timeLable = new QLabel(widget);
        timeLable->setObjectName(QStringLiteral("timeLable"));
        timeLable->setGeometry(QRect(250, 20, 51, 20));
        hieu_Xuat = new QLabel(widget);
        hieu_Xuat->setObjectName(QStringLiteral("hieu_Xuat"));
        hieu_Xuat->setGeometry(QRect(160, 40, 121, 20));
        graphicsView = new QGraphicsView(SorterView);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        graphicsView->setGeometry(QRect(0, 0, 315, 190));

        retranslateUi(SorterView);

        QMetaObject::connectSlotsByName(SorterView);
    } // setupUi

    void retranslateUi(QFrame *SorterView)
    {
        SorterView->setWindowTitle(QApplication::translate("SorterView", "Frame", 0));
        name->setText(QApplication::translate("SorterView", "Thu\341\272\255t to\303\241n", 0));
        swapped->setText(QApplication::translate("SorterView", "S\341\273\221 l\341\272\247n ho\303\241n \304\221\341\273\225i: 0", 0));
        steps->setText(QApplication::translate("SorterView", "S\341\273\221 b\306\260\341\273\233c duy\341\273\207t: 0", 0));
        TexLable->setText(QApplication::translate("SorterView", "Th\341\273\235i gian duy\341\273\207t", 0));
        timeLable->setText(QApplication::translate("SorterView", "00:00:00", 0));
        hieu_Xuat->setText(QApplication::translate("SorterView", "Hi\341\273\207u Xu\341\272\245t: 0 %", 0));
    } // retranslateUi

};

namespace Ui {
    class SorterView: public Ui_SorterView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SORTERVIEW_H
