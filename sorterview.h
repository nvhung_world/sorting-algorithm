#ifndef SORTERVIEW_H
#define SORTERVIEW_H

#include <QFrame>
#include <QGraphicsView>
#include <QTime>
#include "sorter.h"
#include "graphic.h"
#include <QString>
namespace Ui {
class SorterView;
}

class SorterView : public QFrame
{
    Q_OBJECT

public:
    explicit SorterView(QString name, Sorter *sorter, QWidget *parent = 0);
    ~SorterView();
    Sorter *sorter;
    Graphic *graphic;
    QTime runTime,deltaTime;
    int speed;
    int timeId;
    void SetItem(Item *i, bool ascending = true);
    void Play(int speed);
    void PlayToEnd();
    void timerEvent(QTimerEvent *event);
private:
    Ui::SorterView *ui;
};

#endif // SORTERVIEW_H
