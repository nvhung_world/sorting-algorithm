#include "quicksort.h"
QuickSort::QuickSort(): Sorter()
{
}

QuickSort::QuickSort(QuickSort *s):Sorter(s)
{
    index = s->index;
    key_Index= s->key_Index;
    key_Value = s->key_Value;
    list = QList<int>(s->list);
}

QuickSort::~QuickSort()
{
    if(item != 0)
    {
        delete item;
        item = 0;
    }
}

void QuickSort::SortToEnd()
{
    while (list.count()> 1)
    {
        Item *firt = item->GetItem(list[0]);
        if(key_Index< list[0] || key_Index>list[1])
        {
            key_Index = (list[0] + list[1])/2;
            key_Value = firt->Get(key_Index - list[0]);
            index = list[0];
        }
        for (int i = index; i <= list[1]; ++i)
        {
            if(i<key_Index)
            {
                steps++;
                int val = firt->Get(i - list[0]);
                if(!IsRight(val,key_Value))
                {
                    firt->Remove(i - list[0]);
                    firt->Inset(key_Index - list[0],val);
                    swapped++;
                    i--;
                    key_Index--;
                }
            }else if(i>key_Index)
            {
                steps++;
                int val = firt->Get(i - list[0]);
                if(!IsRight(key_Value,val))
                {
                    firt->Inset(key_Index - list[0],val);
                    firt->Remove(i+1 - list[0]);
                    swapped++;
                    key_Index++;
                }
            }
        }
        if(list[1] - list[0]<=2)
            list.removeAt(0);
        else
        {
            if(key_Index != list[1])
                list.insert(1,key_Index);
            else
                list.insert(1,key_Index -1);
        }
        key_Index = -1;
    }
}

int QuickSort::Sort()
{
    if(list.count()<= 1)
        return 1;
    Item *firt = item->GetItem(list[0]);
    if(key_Index< list[0] || key_Index>list[1])
    {
        key_Index = (list[0] + list[1])/2;
        key_Value = firt->Get(key_Index - list[0]);
        index = list[0];
    }
    if(index<key_Index)
    {
        steps++;
        int val = firt->Get(index - list[0]);
        if(!IsRight(val,key_Value))
        {
            firt->Remove(index - list[0]);
            firt->Inset(key_Index - list[0],val);
            firt->GetItem(key_Index - list[0])->status = 1;
            swapped++;
            index--;
            key_Index--;
            item->GetItem(key_Index)->status = 2;
        }
    }else if(index>key_Index)
    {
        steps++;
        int val = firt->Get(index - list[0]);
        if(!IsRight(key_Value,val))
        {
            firt->Inset(key_Index - list[0],val);
            firt->GetItem(key_Index - list[0])->status = 2;
            firt->Remove(index+1 - list[0]);
            swapped++;
            key_Index++;
            item->GetItem(key_Index)->status = 1;
        }
    }
    index++;
    if(index > list[1])
    {
        if(list[1] - list[0]<=2)
            list.removeAt(0);
        else
        {
            if(key_Index != list[1])
                list.insert(1,key_Index);
            else
                list.insert(1,key_Index -1);
        }
        key_Index = -1;
    }
    return 0;
}

void QuickSort::Reset(Item *i, bool ascending)
{
    if(item != 0)
        delete this->item;
    item = new Item(i);
    this->ascending = ascending;

    list.clear();
    list<<0;
    list<<item->Length()-1;
    index = 0;
    key_Index = -1;
    key_Value = 0;

    steps = 0;
    swapped = 0;
}

