#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H

#include "item.h"
#include "sorter.h"
class InsertionSort : public Sorter
{
    public:
        InsertionSort();
        InsertionSort(InsertionSort *s);
        ~InsertionSort();
        void SortToEnd();
        int Sort();
        void Reset(Item *i,bool ascending = true);
    private:
        int i_index,j_index, length;
};

#endif // SELECTIONSORT_H
