#include "graphic.h"
Graphic::Graphic(QObject *parent) : QGraphicsScene(0,0,0,0,parent)
{
    length = new int(0);
    list = new QGraphicsRectItem*[300];
    for(int i = 0; i<300; i++)
       list[i] = 0;
    pen0 = QPen(Qt::black, 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin);
    pen1 = QPen(Qt::blue, 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin);
    pen2 = QPen(Qt::red, 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin);

    brush0 = QBrush(Qt::white);
    brush1 = QBrush(Qt::blue);
    brush2 = QBrush(Qt::red);
}

void Graphic::Raw(Item *item)
{
    int itemLength = item->Length();
    int max = *length > itemLength ? *length  : itemLength;
    int  width = 300/(itemLength> 1 ? itemLength: 1);
    QPen *pen;
    QBrush *brush;
    for(int i =0; i<max; i++)
    {
        switch (item->GetStatus(i))
        {
            case 1:
                pen = &pen1;
                brush = &brush1;
                item->GetItem(i)->status = 0;
                break;
            case 2:
                pen = &pen2;
                brush = &brush2;
                item->GetItem(i)->status = 0;
                break;
            default:
                pen = &pen0;
                brush = &brush0;
                break;
        }
        if(i >= *length)
        {
            list[i] = addRect(width * i,185 - item->Get(i),(int)width,item->Get(i),*pen,*brush);
            (*length)++;
        }
        if(i < itemLength && i < *length)
        {
            list[i]->setRect(width * i,185 - item->Get(i),width,item->Get(i));
            list[i]->show();
            list[i]->setPen(*pen);
            list[i]->setBrush(*brush);
        }
        if(i>= itemLength)
            list[i]->hide();
    }
    setSceneRect(0,0,width * itemLength,187);
}

Graphic::~Graphic()
{
//    for(int i = 0; i<300; i++)
//        if(list[i] != 0)
//            delete list[i];
    delete []list;
    delete length;
}

