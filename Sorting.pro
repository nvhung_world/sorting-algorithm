#-------------------------------------------------
#
# Project created by QtCreator 2016-03-07T11:36:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = "SortAlgorithm"
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    item.cpp \
    bubblesort.cpp \
    sorterview.cpp \
    graphic.cpp \
    insertionsort.cpp \
    quicksort.cpp \
    shellsort.cpp

HEADERS  += mainwindow.h \
    item.h \
    sorter.h \
    bubblesort.h \
    sorterview.h \
    graphic.h \
    insertionsort.h \
    quicksort.h \
    shellsort.h

FORMS    += mainwindow.ui \
    sorterview.ui

RESOURCES +=
win32:
    RC_ICONS += Sort.ico
    VERSION +=  1.1.0
    QMAKE_TARGET_COMPANY += Kem Company
    QMAKE_TARGET_DESCRIPTION += "Sort Algorithm"
    QMAKE_TARGET_COPYRIGHT += "Nguyễn Văn Hưng : 27/03/2016"
