#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLayout>
#include <QString>
#include <qmath.h>
#include <QCheckBox>
#include "sorterview.h"
#include "insertionsort.h"
#include "bubblesort.h"
#include "quicksort.h"
#include "shellsort.h"
#include "item.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    SorterView *View[4];
    InsertionSort *insertionSort;
    BubbleSort *bubbleSort;
    QuickSort *quickSort;
    ShellSort * shellSort;
    Item *item;
    bool isPlay;

private slots:
    void on_pushButton_2_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_horizontalSlider_2_valueChanged(int value);

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_checkBox_clicked();

    void on_checkBox_2_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
