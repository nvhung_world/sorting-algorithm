#include "sorterview.h"
#include "ui_sorterview.h"
SorterView::SorterView(QString name, Sorter *sorter, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::SorterView)
{
    ui->setupUi(this);
    this->sorter = sorter;
    ui->name->setText("Thuật Toán: " + name);
    speed = -1;
    timeId = -1;
    runTime = QTime(0,0,0,0);
    graphic = new Graphic();
    ui->graphicsView->setScene(graphic);
}

SorterView::~SorterView()
{
    delete ui;
    delete graphic;
}

void SorterView::SetItem(Item *i, bool ascending)
{
    sorter->Reset(i,ascending);
    if(timeId > -1)
    {
        killTimer(timeId);
        speed = -1;
        timeId = -1;
    }
    graphic->Raw(sorter->item);

    runTime = QTime(0,0,0,0);
    ui->timeLable->setText("Thời gian duyệt: 00:00:00");
    ui->steps->setText("Số bước duyệt: " + QString::number(0,10));
    ui->swapped->setText("Số lần hoán đổi: " + QString::number(0,10));
    ui->hieu_Xuat->setText("Hiệu Xuất: 0 %");
}

void SorterView::Play(int speed)
{
    if(speed > 0)
    {
        if(this->speed != speed)
        {
            if(timeId > -1)
                killTimer(timeId);
            timeId = startTimer(1000/speed);
            this->speed = speed;
        }
        deltaTime = QTime(0,0,0,0);
        deltaTime.start();

    }else if(timeId > -1)
        {
            killTimer(timeId);
            this->speed = -1;
            timeId = -1;
        }
}

void SorterView::PlayToEnd()
{
    deltaTime = QTime(0,0,0,0);
    deltaTime.start();
    sorter->SortToEnd();
    runTime = runTime.addMSecs(deltaTime.elapsed());
    deltaTime.restart();

    graphic->Raw(sorter->item);

    if(timeId > -1)
    {
        killTimer(timeId);
        speed = -1;
        timeId = -1;
    }

    ui->timeLable->setText(runTime.toString("mm:ss.zzz"));
    ui->steps->setText("Số bước duyệt: " + QString::number( this->sorter->steps,10));
    ui->swapped->setText("Số lần hoán đổi: " + QString::number( this->sorter->swapped,10));
    ui->hieu_Xuat->setText("Hiệu Xuất: " + QString::number(100*(double)sorter->item->Length()/(sorter->steps<=0?1: sorter->steps)) + " %" );
}

void SorterView::timerEvent(QTimerEvent *event)
{
    if(sorter->Sort() == 1)
    {
        graphic->Raw(sorter->item);
        return;
    }
    runTime = runTime.addMSecs(deltaTime.elapsed());
    deltaTime.restart();
    graphic->Raw(sorter->item);

    ui->timeLable->setText(runTime.toString("mm:ss.zzz"));
    ui->steps->setText("Số bước duyệt: " + QString::number( this->sorter->steps,10));
    ui->swapped->setText("Số lần hoán đổi: " + QString::number( this->sorter->swapped,10));
    ui->hieu_Xuat->setText("Hiệu Xuất: " + QString::number(100*(double)sorter->item->Length()/(sorter->steps<=0?1: sorter->steps)) + " %" );
}
