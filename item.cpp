#include "item.h"

Item::Item()
{
    status = 0;
    value = 0;
    Next = 0;
}

Item::Item(int value)
{
    this->value = value;
    status = 0;
    Next = new Item();
}

Item::Item(Item *s)
{
    value = s->value;
    status = s->status;
    if(s->Next != 0)
        Next = new Item(s->Next);
    else
        Next = 0;
}

Item::~Item()
{
    if(Next != 0)
    {
        delete Next;
        Next = 0;
    }
}

void Item::Set(int index, int value)
{
    if(index == 0)
        this->value = value;
    else if(Next != 0)
        Next->Set(index- 1,value);
}

int Item::Get(int index)
{
    if(index == 0)
        return value;
    else if(Next != 0)
        return Next->Get(index- 1);
    else
        return 0;
}

int Item::GetStatus(int index)
{
    if(index == 0)
    {

        return status;
    }
    else if(Next != 0)
        return Next->GetStatus(index- 1);
    else
        return 0;
}

Item *Item::GetItem(int index)
{
    if(index == 0)
        return this;
    else if(Next != 0)
        return Next->GetItem(index- 1);
    else
        return 0;
}

int Item::Length()
{
    if(Next != 0)
        return 1 + Next->Length();
    else
        return 0;
}

void Item::Add(int value)
{
    if(Next == 0)
    {
        this->value = value;
        status = 0;
        Next = new Item();
    }
    else
        Next->Add(value);
}

void Item::Add(Item *s)
{
    if(Next == 0)
    {
        Next = s;
        if(s->Next == 0)
            s->Next = new Item();
    }
    else
        Next->Add(s);
}

void Item::Inset(int index, int value)
{
    if(index <= 0)
    {
        Item *i = new Item();
        i->value = this->value;
        i->status = this->status;
        i->Next = Next;
        Next = i;
        this->value = value;
        status = 0;
    }
    else if(Next != 0)
        Next->Inset(index -1, value);

}

void Item::Remove(int index)
{
    if(index <= 0)
    {
        Item *i = Next;
        if(i!=0)
        {
            status = i->status;
            value = i->value;
            Next = i->Next;
            i->Next = 0;
            delete i;
        }
        else
        {
            status = 0;
            value = 0;
            Next = 0;
        }
    }
    else if(Next != 0)
        Next->Remove(index -1);
}

