#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <item.h>
class Graphic : public QGraphicsScene
{
public:
    Graphic(QObject *parent = 0);
    void Raw(Item *i);
    QGraphicsRectItem **list;
    int *length;
    ~Graphic();
    QPen pen0, pen1,pen2;
    QBrush brush0, brush1, brush2;
};

#endif // GRAPHIC_H
