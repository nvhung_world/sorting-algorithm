#include "shellsort.h"

ShellSort::ShellSort():Sorter()
{
}

ShellSort::ShellSort(ShellSort *s):Sorter(s)
{

}

ShellSort::~ShellSort()
{
    if(item != 0)
    {
        delete item;
        item = 0;
    }
}

void ShellSort::SortToEnd()
{
    while(h!=0)
    {
        for(int i = i_index;i<length;i++)
        {
            steps++;    //tăng số bước duyệt
            tmp = item->Get(i);
            while(j_index > -1 && !IsRight(item->Get(j_index),tmp))
            {
                swapped++;  //tăng số lần đổi chỗ
                item->Set(j_index+h,item->Get(j_index));
                j_index -= h;
            }
            item->Set(j_index+h,tmp);
            j_index = i +1 - h;
        }
        h/=2;
        i_index = h;
    }
}

int ShellSort::Sort()
{
    if(h==0)
        return 1;
    if(i_index< length)
    {
        steps++;    //tăng số bước duyệt
        if(j_index > -1 && !IsRight(item->Get(j_index),tmp))
        {
            swapped++;  //tăng số lần đổi chỗ
            item->Set(j_index+h,item->Get(j_index));
            item->GetItem(j_index+h)->status = 1;
            item->GetItem(j_index)->status = 2;
            j_index -= h;
        }
        else
        {
            item->Set(j_index+h,tmp);
            i_index ++;
            tmp = item->Get(i_index);
            j_index = i_index - h;
        }
    }
    else
    {
        h/=2;
        i_index = h;
        tmp = item->Get(i_index);
        j_index = i_index - h;
    }
    return 0;
}
void ShellSort::Reset(Item *i, bool ascending)
{
    if(item != 0)
        delete this->item;
    item = new Item(i);
    this->ascending = ascending;
    length = item->Length();
    i_index = h = length/2;
    tmp = item->Get(i_index);
    j_index = 0;

    steps = 0;
    swapped = 0;
}

