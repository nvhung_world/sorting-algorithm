#ifndef QUICKSORT_H
#define QUICKSORT_H
#include "sorter.h"
#include <QList>
class QuickSort :public Sorter
{
public:
    QuickSort();
    QuickSort(QuickSort *s);
    ~QuickSort();
    void SortToEnd();
    int Sort();
    void Reset(Item *i,bool ascending = true);
private:
    QList<int> list;
    int index, key_Index, key_Value;
};

#endif // QUICKSORT_H
