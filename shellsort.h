#ifndef SHELLSORT_H
#define SHELLSORT_H

#include "sorter.h"
class ShellSort: public Sorter
{
public:
    ShellSort();
    ShellSort(ShellSort *s);
    ~ShellSort();
    void SortToEnd();
    int Sort();
    void Reset(Item *i,bool ascending = true);
protected:
    int i_index, j_index, length , h, tmp;
};

#endif // SHELLSORT_H
