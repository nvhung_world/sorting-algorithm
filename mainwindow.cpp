#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //
    isPlay = false;
    //
    item = new Item();
    for(int i=0; i<2;i++)
        item->Add(qrand() % 180+1);
    //
    insertionSort = new InsertionSort();
    bubbleSort = new BubbleSort();
    quickSort = new QuickSort();
    shellSort = new ShellSort();
    //
    View[0] = new SorterView("Shell Sort",shellSort, this->ui->frame);
    View[1] = new SorterView("Quick Sort",quickSort, this->ui->frame);
    View[2] = new SorterView("Insertion Sort",insertionSort,this->ui->frame);
    View[3] = new SorterView("Bubble Sort",bubbleSort,this->ui->frame);
    //
    View[0]->move(5,5);
    View[1]->move(330,5);
    View[2]->move(5,265);
    View[3]->move(330,265);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete item;
    delete insertionSort;
    delete bubbleSort;
    delete quickSort;
    delete shellSort;
    for(int i=0;i<4;i++)
        delete View[i];
}

void MainWindow::on_pushButton_2_clicked()      //Random list
{
    if(item != 0)
        delete item;
    item = new Item();
    for(int i=0; i<ui->horizontalSlider->value();i++)
        item->Add(qrand() % 180+1);
    for(int i=0;i<4;i++)
        View[i]->SetItem(item,ui->checkBox->isChecked() );
    ui->pushButton->setText("Play");
    ui->pushButton->setEnabled(true);
    ui->pushButton_3->setEnabled(true);
    ui->checkBox->setEnabled(true);
    ui->checkBox_2->setEnabled(true);
    isPlay = false;
}

void MainWindow::on_horizontalSlider_valueChanged(int value)        //Thay đổi số phần tử
{
    ui->label->setText("Số phần tử: " + QString::number(value,10));
    on_pushButton_2_clicked();
}

void MainWindow::on_horizontalSlider_2_valueChanged(int value)      //Thay đổi vận tốc play
{
    ui->label_2->setText("Vận tốc: " + QString::number(value,10));
    if(isPlay == true)
    {
        for(int i=0;i<4;i++)
            View[i]->Play(ui->horizontalSlider_2->value());
    }
}

void MainWindow::on_pushButton_3_clicked()      //Play To End
{
    for(int i=0;i<4;i++)
        View[i]->PlayToEnd();
    ui->pushButton->setText("Play");
    ui->pushButton->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
}

void MainWindow::on_pushButton_clicked()        //Play
{
    if(isPlay == true)
    {
        for(int i=0;i<4;i++)
            View[i]->Play(-1);
        ui->pushButton->setText("Play");
        isPlay = false;
    }else
    {
        for(int i=0;i<4;i++)
            View[i]->Play(ui->horizontalSlider_2->value());
        ui->pushButton->setText("Pause");
        isPlay = true;
    }
}

void MainWindow::on_checkBox_clicked()      // Thay đổi thứ tự sắp xếp
{
    for(int i=0;i<4;i++)
        View[i]->SetItem(item,ui->checkBox->isChecked() );
    ui->pushButton->setText("Play");
    ui->pushButton->setEnabled(true);
    ui->pushButton_3->setEnabled(true);
    isPlay = false;
}

void MainWindow::on_checkBox_2_clicked()    // Thay đổi thứ tự sắp xếp
{
    for(int i=0;i<4;i++)
        View[i]->SetItem(item,ui->checkBox->isChecked() );
    ui->pushButton->setText("Play");
    ui->pushButton->setEnabled(true);
    ui->pushButton_3->setEnabled(true);
    isPlay = false;
}
