#ifndef BUBBLESORT_H
#define BUBBLESORT_H

#include "sorter.h"
class BubbleSort : public Sorter
{
    public:
        BubbleSort();
        BubbleSort(BubbleSort *b);
        ~BubbleSort();
        void SortToEnd();
        int Sort();
        void Reset(Item *i,bool ascending = true);
    private:
        int i_index,j_index, length;
};

#endif // BUBBLESORT_H
