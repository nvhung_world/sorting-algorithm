 #ifndef SITEM_H
#define SITEM_H


class Item
{
    public:
        Item();
        Item(int value);
        Item(Item * s);
        ~Item();
        int value, status;
        Item *Next;
        void Set(int index,int value);
        int Get(int index);
        int GetStatus(int index);
        Item *GetItem(int index);
        int Length();
        void Add(int value);
        void Add(Item *s);
        void Inset(int index,int value);
        void Remove(int index);
};

#endif // SITEM_H
