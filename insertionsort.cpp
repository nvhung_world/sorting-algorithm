#include "insertionsort.h"

InsertionSort::InsertionSort() :Sorter()
{
    i_index = 1;
    j_index = 0;
}

InsertionSort::InsertionSort(InsertionSort *s) : Sorter(s)
{
    i_index = s->i_index;
    j_index = s->j_index;
    length = item->Length();
}

InsertionSort::~InsertionSort()
{
    if(item != 0)
    {
        delete item;
        item = 0;
    }
}

void InsertionSort::SortToEnd()
{
    Item *a, *b;
    //duyệt từng phần tử
    for(int i = i_index;i < length;i++)
    {
        b = item->GetItem(i);
        //tìm vị trí chính xác của phần tử đang chọn
        for(int j = j_index;j<i;j++)
        {
            steps++;    //tăng số bước duyệt
            a = item->GetItem(j);
            if(!IsRight(a,b))   //Kiểm tra vị trí a và b là đúng hay sai
            {
                swapped++;  //tăng số lần đổi chỗ
                int val = b->value;
                item->Remove(i);
                item->Inset(j,val);
                i_index++;
                j_index = 0;
                break;
            }
            else
                j_index++;
        }
        j_index = 0;
        i_index++;
    }
}

int InsertionSort::Sort()
{
    if(i_index >= length) //Nếu i_index vượt quá chỉ số của mảng
        return 1;
    if(j_index >= i_index)  //Nếu j_index vượt quá i_index
    {
        if(i_index < length -1)
            i_index++;
        else
            return 1;
        j_index = 0;
    }
    //Kiểm tra hai phần tử a và b
    Item *b = item->GetItem(i_index);
    Item *a = item->GetItem(j_index);
    steps++;    //tăng số bước duyệt
    if(!IsRight(a,b))   //Kiểm tra vị trí a và b là đúng hay sai
    {
        swapped++;  //tăng số lần đổi chỗ
        int val = b->value;
        item->Remove(i_index);
        item->Inset(j_index,val);
        item->GetItem(j_index)->status = 2;
        i_index++;
        j_index = 0;
    }
    else
        j_index++;
    return 0;
}

void InsertionSort:: Reset(Item *i,bool ascending)
{
    if(item != 0)
        delete this->item;
    item = new Item(i);
    this->ascending = ascending;
    i_index = 1;
    j_index = 0;
    length = item->Length();

    steps = 0;
    swapped = 0;
}
